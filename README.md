<h1 align="center">Ahoy!</h1>

<p align="center">
	<a href="https://github.com/DeclanChidlow">
	<img src="https://img.shields.io/badge/GitHub-181717?style=for-the-badge&logo=GitHub&logoColor=white" alt="GitHub"></a>
	<a href="https://fedi.vale.rocks/vale">
		<img src="https://img.shields.io/badge/Fediverse-F1007E?style=for-the-badge&logo=ActivityPub&logoColor=white" alt="Fediverse"></a>
 	<a href="https://bsky.app/profile/vale.rocks">
		<img src="https://img.shields.io/badge/Bluesky-0085ff?style=for-the-badge&logo=Bluesky&logoColor=white" alt="Bluesky"></a>
	<a href="https://codepen.io/OuterVale">
    		<img src="https://img.shields.io/badge/CodePen-white?style=for-the-badge&logo=CodePen&logoColor=black" alt="CodePen"></a>
	<a href="https://news.ycombinator.com/user?id=OuterVale">
    		<img src="https://img.shields.io/badge/Hacker_News-F0652F?style=for-the-badge&logo=YCombinator&logoColor=white" alt="Hacker News"></a>
</p>
<p>
I'm a front-end developer with a skew to the front of the front-end. My expertise lies in core web technologies — HTML, CSS, and JavaScript — and leveraging them to craft digital experiences that are both functional and visually appealing.

When not bodging together scripts, you may find me cruising around on my unicycle, writing for [my website](https://vale.rocks/?utm_source=GitLabBio), browsing the crevices of cyberspace, or inadvertently converting an otherwise functional piece of tech into a paperweight.
</p>

<h2 align="center">Like what you see?</h2>

If you enjoy my work, then [consider supporting me financially](https://vale.rocks/support?utm_source=GitLabBio). \
If you're particularly impressed, I'm always open to work. Get in touch [through my website](https://vale.rocks/contact?utm_source=GitLabBio).